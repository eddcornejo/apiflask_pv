#!/usr/bin/env python
# -*- coding: utf-8 -*-
from flask_script import Manager
from aplicacion.app import app,db
from aplicacion.modelos import *
from getpass import getpass

manager = Manager(app)
app.config['DEBUG'] = True # Ensure debugger will load.

@manager.command
def create_tables():
    "Crear tablas en la base de datos."
    db.create_all()

@manager.command
def drop_tables():
    "Eliminar todas las tablas relacionadas de la base de datos."
    db.drop_all()

@manager.command
def add_data_tables():
    db.create_all()
    # Añadir set de datos de prueba para los modelos que se requiera


@manager.command
def create_admin():
    usuario={"username":input("Usuario:"),
            "password":getpass("Password:"),
            "nombre":input("Nombre completo:"),
            "email":input("Email:"),
            "admin": True}
    usu=Usuarios(**usuario)
    db.session.add(usu)
    db.session.commit()

if __name__ == '__main__':
    manager.run()
