#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys,os,click

#sys.path.insert(0, os.path.join( os.path.dirname(os.path.abspath(__file__)) ,'modelos'))
from flask import Flask, request
from flask_restful import Api
from config import app_config
from db import db

# IMPORTACIÓN DE RECURSOS

app = Flask(__name__)
db.init_app(app)

#Se establece enviroment como argumento
#enviroment = sys.argv[1]
enviroment ="development"

#Se setean variables de configuracion segun ambiente(env)
app.config.from_object(app_config[enviroment])
api = Api(app)

# SE DEFINEN LOS ENDPOINTS Y LA CLASE QUE SE ENCARGARÁ DE PROCESAR CADA SOLICITUD
